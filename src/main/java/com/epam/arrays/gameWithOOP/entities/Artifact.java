package com.epam.arrays.gameWithOOP.entities;

import com.epam.arrays.gameWithOOP.services.EntityService;

public class Artifact extends Entity implements Powerable {

	public Artifact() {
		generatePower();
	}

	@Override
	public void generatePower() {
		setPower(EntityService.getInstance().generatePower(this));
	}
}
