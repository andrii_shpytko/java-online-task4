package com.epam.arrays.gameWithOOP.entities;

import com.epam.arrays.gameWithOOP.services.EntityService;

public class Monster extends Entity implements Powerable {

	public Monster() {
		generatePower();
	}

	@Override
	public void generatePower() {
		setPower(EntityService.getInstance().generatePower(this));
	}
}
