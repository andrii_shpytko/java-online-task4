package com.epam.arrays.gameWithOOP.entities;

public class Hero extends Entity {
	private static final int HERO_START_POWER = 25;

	public Hero(){
		setPower(HERO_START_POWER);
	}
}
