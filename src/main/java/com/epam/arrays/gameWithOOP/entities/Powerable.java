package com.epam.arrays.gameWithOOP.entities;

public interface Powerable {

	void generatePower();
}
