package com.epam.arrays.gameWithOOP.hall;

import com.epam.arrays.gameWithOOP.entities.Powerable;
import com.epam.arrays.gameWithOOP.services.EntityService;

public class Door {
	private Powerable entity;
	private Status status;

	public Door() {
		this.entity = createdEntity();
		status = Status.NEW;
	}

	private Powerable createdEntity() {
		return EntityService.createEntity();
	}

	public Powerable getEntity() {
		return entity;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
