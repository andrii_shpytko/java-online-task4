package com.epam.arrays.arrayTask2;

import java.util.Arrays;

/**
 * Delete all numbers that are repeated more than twice in the array.
 */
public class AppArray {
    public static void main(String[] args) {
        String line = "2 4 6 1 2 11 45 6 99 102 hello 2 85 6 1024 45 5.Bob 990 55 45";
        int[] numbers = arrayIntegers(line);
        printNumberDuplicatedElements(numbers);
        System.out.println(Arrays.toString(removeDuplicates(numbers)));
    }

    private static int[] arrayIntegers(String line) {
        String[] integerString = line.split(" ");
        int[] integers = new int[integerString.length];
        for (int i = 0; i < integerString.length; i++) {
            try {
                integers[i] = Integer.parseInt(integerString[i]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return integers;
    }

    private static int[] removeDuplicates(int[] values) {
        boolean[] masksBoolean = new boolean[values.length];
        int removeCount = 0;
        for (int i = 0; i < values.length; i++) {
            if (!masksBoolean[i]) {
                int temporary = values[i];
                for (int j = i + 1; j < values.length; j++) {
                    if (temporary == values[j]) {
                        masksBoolean[j] = true;
                        removeCount++;
                    }
                }
            }
        }
        int[] result = new int[values.length - removeCount];
        for (int i = 0, j = 0; i < values.length; i++) {
            if (!masksBoolean[i]) {
                result[j++] = values[i];
            }
        }
        return result;
    }

    private static void printNumberDuplicatedElements(int[] values){
        boolean[] duplicatedElement = new boolean[values.length];
        System.out.println(Arrays.toString(values));
        for(int i = 0; i < values.length-1; i++){
            for(int j = i + 1; j <values.length; j++ ){
                if(!duplicatedElement[j] && values[i] == values[j]){
                    duplicatedElement[j] = true;
                    System.out.println("\tDuplicated element : " + values[j]);
                    System.out.println("\tIndex of the duplicated element : " + j);
                }
            }
        }
    }


}
