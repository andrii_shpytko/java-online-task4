package com.epam.arrays.gameWithoutOOP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class StartGame {
    private static Random random = new Random();
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static final String MONSTER = "monster";
    private static final String ARTIFACT = "artifact";

    public static void main(String[] args) {
        System.out.println("You win: " + countStrength(createDoors(10)));
    }

    private static int countStrength(String[] doors) {
        int[] strengthArray = createStrength(doors);
        int strength = 25;
        showDoors(doors);
        showNumberDoors(doors);
        showStrength(strengthArray);
        for (int i = 0; i < doors.length; i++) {
            if (i == doors.length - 2) { //забив цвях!
                i = 0;
            }
            int strengthObject = 0;
            int numberOfDoor = selectDoor(br, doors);
            System.out.println("\t\tYou select the door number: " + numberOfDoor);
            if (ARTIFACT.equalsIgnoreCase(doors[numberOfDoor])) {
                System.out.printf("You strength = %d\n", strength);
                strengthObject = strengthArray[numberOfDoor];
                System.out.print("Congratulate! You found an ARTIFACT. ");
                System.out.println("This ARTIFACT has strength: " + strengthObject);
                strength += strengthObject;
                System.out.printf("You strength increase and = %d\n", strength);
                doors = changeQuantityDoors(doors, numberOfDoor);
                strengthArray = changeQuantityStrength(strengthArray, numberOfDoor);
                showDoors(doors);
                showNumberDoors(doors);
                showStrength(strengthArray);
            } else if (MONSTER.equalsIgnoreCase(doors[numberOfDoor])) {
                System.out.println("Be careful! You found a MONSTER. You strength = " + strength);
                strengthObject = strengthArray[numberOfDoor];
                System.out.println("Monster has strength: " + strengthObject);
                doors = changeQuantityDoors(doors, numberOfDoor);
                strengthArray = changeQuantityStrength(strengthArray, numberOfDoor);
                if (strength >= strengthObject) {
                    System.out.println("Congratulate! You are stronger than the MONSTER");
                } else {
                    System.out.println("Sorry you lost! You strength is less than a MONSTER");
                }
                showDoors(doors);
                showNumberDoors(doors);
                showStrength(strengthArray);
            }
        }
        return strength;
    }

    private static int generateNumber() { // getChoice
        int resultOfGeneration = random.nextInt(2);
        return resultOfGeneration;
    }

    private static String[] createDoors(int quantity) {
        String[] doors = new String[quantity];
        for (int i = 0; i < doors.length; i++) {
            if (generateNumber() == 1) {
                doors[i] =/* i + "." + */ARTIFACT;
            } else {
                doors[i] =/* i + "." +*/ MONSTER;
            }
        }
        return doors;
    }

    private static int[] createStrength(String[] doors) {
        int[] strength = new int[doors.length];
        for (int i = 0; i < strength.length; i++) {
            strength[i] = generateStrength(doors[i]);
        }
        return strength;
    }

    private static int generateStrength(String door) {
        int strength = 0;
        switch (door.toLowerCase()) {
            case "artifact":
                strength = 10 + random.nextInt(80 - 10) + 1;
                break;
            case "monster":
                strength = 5 + random.nextInt(100 - 5) + 1;
                break;
        }
        return strength;
    }

    private static int selectDoor(BufferedReader br, String[] doors) {
        int number = 0;
        boolean continueLoop = true;
        do {
            System.out.print("Input number of doors: ");
            number = validateNumber(br);
            if (number >= 0 && number < doors.length) {
                continueLoop = false;
                return number;
            } else {
                System.out.println("\tSorry! Input correct number: ");
            }
        } while (continueLoop);

        return 0;
    }

    private static int validateNumber(BufferedReader br) {
        int number = 0;
        boolean continueLoop = true;
        do {
            try {
                number = Integer.parseInt(br.readLine());
                continueLoop = false;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println("Exception " + e);
                e.printStackTrace();
                System.out.print("You input incorrect value! Please input Integer value: ");
            }
        } while (continueLoop);
        return number;
    }

    private static String[] changeQuantityDoors(String[] oldArray, int oldIndex) {
//        System.out.println("\t\t\tThis method uses!");
        String[] newArray;
        for (int i = 0; i < oldArray.length; i++) {
            if (i == oldIndex) {
                newArray = new String[oldArray.length - 1];
                System.arraycopy(oldArray, 0, newArray, 0, i);
                System.arraycopy(oldArray, i + 1, newArray, i, oldArray.length - i - 1);
                return newArray;
            }
        }
        return oldArray;
    }

    private static int[] changeQuantityStrength(int[] oldArray, int oldIndex) {
//        System.out.println("\t\t\tThis method uses!");
        int[] newArray;
        for (int i = 0; i < oldArray.length; i++) {
            if (i == oldIndex) {
                newArray = new int[oldArray.length - 1];
                System.arraycopy(oldArray, 0, newArray, 0, i);
                System.arraycopy(oldArray, i + 1, newArray, i, oldArray.length - i - 1);
                return newArray;
            }
        }
        return oldArray;
    }

    private static void showDoors(String[] doors) {
        for (int i = 0; i < doors.length; i++) {
            System.out.print(i + "." + doors[i] + "\t");
        }
        System.out.println();
    }

    private static void showStrength(int[] strength) {
        for (int i = 0; i < strength.length; i++) {
            System.out.print(i + "). = " + strength[i] + "\t");
        }
        System.out.println();
    }

    private static void showNumberDoors(String[] doors) {
        for (int i = 0; i < doors.length; i++) {
            System.out.print(i + "\t");
        }
        System.out.println();
    }


}
