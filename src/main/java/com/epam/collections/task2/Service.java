package com.epam.collections.task2;

import java.util.Collections;
import java.util.List;

public class Service {

    public void sortedByNameCountry(List<Country> countries) {
        System.out.println("\tPrint countries sorted by name of country:");
        Collections.sort(countries, Country.getByCountryName());
        countries.forEach(((Country country) -> System.out.println(country)));
    }

    public void sortedByNameCapital(List<Country> countries) {
        System.out.println("\tPrint countries sorted by name of capital:");
        Collections.sort(countries, Country.getByCapitalName());
        countries.forEach(((Country country) -> System.out.println(country)));
    }
}
