package com.epam.collections.task2;

import java.util.Comparator;

public class Country {
    private static final Comparator<Country> BY_COUNTRY_NAME =
            (Country c1, Country c2) -> c1.getNameCountry().compareToIgnoreCase(c2.getNameCountry());

    private static final Comparator<Country> BY_CAPITAL_NAME =
            (Country c1, Country c2) -> c1.getNameCapital().compareToIgnoreCase(c2.getNameCapital());


    private String nameCountry;
    private String nameCapital;

    public Country(String nameCountry, String nameCapital) {
        this.nameCountry = nameCountry;
        this.nameCapital = nameCapital;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getNameCapital() {
        return nameCapital;
    }

    public void setNameCapital(String nameCapital) {
        this.nameCapital = nameCapital;
    }

    public static Comparator<Country> getByCountryName() {
        return BY_COUNTRY_NAME;
    }

    public static Comparator<Country> getByCapitalName() {
        return BY_CAPITAL_NAME;
    }

    @Override
    public String toString() {
        return String.format("Country{nameCountry = %s, nameCapital = %s}", nameCountry, nameCapital);
    }

}
