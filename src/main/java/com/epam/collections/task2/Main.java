package com.epam.collections.task2;

import java.util.ArrayList;
import java.util.List;

/**
 * Task:
 * Create a class containing two String objects and make it Comparable so that the comparison only cares about
 * the first String. Fill an array and an ArrayList with objects of your class by using a custom generator
 * (eg, which generates pairs of Country-Capital). Demonstrate that sorting works properly.
 * Now make a Comparator that only cares about the second String and demonstrate that sorting works properly.
 * Also perform a binary search using your Comparator..
 */
public class Main {
    public static void main(String[] args) {
        Service service = new Service();
        List<Country> countryList = new ArrayList<>();
        countryList.add(new Country("Thailand", "Bangkok"));
        countryList.add(new Country("Madagascar", "Antananarivo"));
        countryList.add(new Country("Bahamas", "Nassau"));
        countryList.add(new Country("Canada", "Ottawa"));
        countryList.add(new Country("Cuba", "Havana"));
        countryList.add(new Country("Cuba", "Havana"));
        countryList.add(new Country("Portugal", "Lisbon"));
        countryList.add(new Country("Mozambique", "Maputo"));

        service.sortedByNameCountry(countryList);
        service.sortedByNameCapital(countryList);

    }

}
